<?php
use \Firebase\JWT\JWT;

require __DIR__ . '/vendor/autoload.php';

header ( 'Access-Control-Allow-Origin: http://localhost:4200' );
header ( 'Access-Control-Allow-Credentials: true' );
header ( 'Access-Control-Allow-Headers: Authorization' );
header ( 'Cache-Control: no-cache, must-revalidate' ); // HTTP 1.1

function fail($message) {
	// echo $message;
	// header('HTTP/1.1 500 Internal Server Error: ' + $message);
	// exit;
}
function decodeToken($idToken) {
	$uri = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com';
	$certsRes = file_get_contents ( $uri );
	$certs = json_decode ( $certsRes );
	$identity = null;
	$error = null;
	
	try {
		$identity = JWT::decode ( $idToken, ( array ) $certs, [ 
				'RS256' 
		] );
	} catch ( \DomainException $e ) {
		$error = 'Domain error: ' . $e->getMessage ();
	} catch ( \UnexpectedValueException $e ) {
		$error = 'Unexpected error: ' . $e->getMessage ();
	} catch ( \SignatureInvalidException $e ) {
		$error = 'Invalid error: ' . $e->getMessage ();
	} catch ( \BeforeValidException $e ) {
		$error = 'error: ' . $e->getMessage ();
	} catch ( \ExpiredException $e ) {
		$error = 'Expired error: ' . $e->getMessage ();
	}
	
	$GLOBALS ['error'] = $error;
	
	return $identity;
}
function getUserRoles($uid) {
	$roles = file_get_contents ( "https://semilla-besada-d6d06.firebaseio.com/user/profile/" . $uid . "/roles.json" );
	
	return ( array ) json_decode ( $roles );
}
function createCustomToken($identity) {
	$file = file_get_contents ( "/home/remi/secret/service-account.json" );
	$serviceAccount = json_decode ( $file, true );
	
	$service_account_email = $serviceAccount ['client_email'];
	$private_key = $serviceAccount ['private_key'];
	$now_seconds = time ();
	$exp = $now_seconds + (60 * 60);
	$uid = $identity->user_id;
	
	$json = file_get_contents ( "https://semilla-besada-d6d06.firebaseio.com/user/profile/" . $uid . "/roles.json" );
	;
	$roles = ( array ) json_decode ( $json );
	
	$payload = array (
			"alg" => "RS252",
			"iss" => $service_account_email,
			"sub" => $service_account_email,
			"aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
			"iat" => $now_seconds,
			"exp" => $exp, // Maximum expiration time is one hour
			"uid" => $uid,
			"claims" => $roles,
			"firebase" => $identity->firebase 
	);
	
	// header("Cache-Control: max-age="+$exp);
	
	return [ 
			'token' => JWT::encode ( $payload, $private_key, "RS256" ) 
	]
	// 'identity' => $identity == null ? "" : (array) $identity,
	;
}

$headers = apache_request_headers ();

JWT::$leeway = 60;

if ($headers ['Authorization'] != null) {
	$authHeader = $headers ['Authorization'];
	$matches = array ();
	$regex = '/^Bearer ((?<header>[a-zA-Z0-9\-_]+)\.(?<payload>[a-zA-Z0-9\-_]+)\.(?<signature>[a-zA-Z0-9\-_]+))$/';
	
	if (preg_match ( $regex, $authHeader, $matches )) {
		
		$idToken = $matches [1];
		$identity = decodeToken ( $idToken );
		
		if ($identity == null)
			echo json_encode ( [ 
					'error' => $GLOBALS ['error'] 
			] );
		
		else {
			$token = createCustomToken ( $identity );
			
			echo json_encode ( $token );
		}
	} 

	else
		echo json_encode ( [ 
				"error" => $headers 
		] );
}
